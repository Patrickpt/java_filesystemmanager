import main.java.userInteraction.SystemLoop;

public class Main {
    public static void main(String[] args) {
        SystemLoop mainLoop = new SystemLoop();
        mainLoop.start();
    }
}
