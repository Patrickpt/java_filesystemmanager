package main.java.userInteraction;

import java.io.File;
import java.util.Scanner;

import main.java.logging.Logger;

public class SystemLoop {
    Logger logger = new Logger();
    String logMessage = "";

    private void listFiles(String url) {
        String[] fileNames;
        File dir = new File(url);
        fileNames = dir.list();
        for(String filename : fileNames){
            System.out.println(filename);
        }
        logMessage = "Listed " + fileNames.length + " files";
    }

    private void listFilesByExtension(String url, String extension){
        String[] fileNames;
        int counter = 0;
        File dir = new File(url);
        fileNames = dir.list();
        for(String filename : fileNames){
            if(filename.endsWith(extension)){
                counter++;
                System.out.println(filename);
            }
        }
        logMessage = "Listed " + counter + " files with the extension " + extension;
    }

    //Create main menu
    public void start(){
        Scanner scanner = new Scanner(System.in);
        boolean cancelled = false;
        long startTime;
        long endTime;
        do {
            System.out.println("Welcome to this file system manager!");
            System.out.println("Please input the number corresponding to what you want to do:");
            System.out.println("\n");
            System.out.println("1. List all files");
            System.out.println("2. List files by their extension");
            System.out.println("3. Manipulate .txt");
            System.out.println("4. Quit");
            int choice = 0;
            try {
                choice = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Error: " + e);
                //Prevents infinite loop on invalid input
                if(scanner.hasNext()){
                    scanner.next();
                }
            }

            switch (choice) {
                case 1:
                    //List all files in resources folder
                    startTime = System.nanoTime();
                    listFiles("../src/main/resources");
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, logMessage);
                    System.out.println("\n");
                    break;
                case 2:
                    //List all files with specific extension in resources folder
                    System.out.println("What extension do you want to search for?");
                    scanner.nextLine();
                    String extension = scanner.nextLine();
                    startTime = System.nanoTime();
                    listFilesByExtension("../src/main/resources", extension);
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, logMessage);
                    System.out.println("\n");
                    break;
                case 3:
                    //Open menu with options for interacting with the .txt file
                    ManipulateTxt manipulateTxt = new ManipulateTxt("../src/main/resources/Dracula.txt", scanner);
                    manipulateTxt.start();
                    System.out.println("\n");
                    break;
                case 4:
                    //Quit
                    cancelled = true;
                    break;
                default:
                    System.out.println("Please enter a valid option!");
                    System.out.println("\n");
                    break;
            }
        } while (!cancelled);
        scanner.close();
    }
}
