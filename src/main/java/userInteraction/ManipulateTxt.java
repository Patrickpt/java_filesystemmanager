package main.java.userInteraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import main.java.logging.Logger;

public class ManipulateTxt {
    File textFile;
    String path;
    Logger logger = new Logger();
    String logMessage = "";
    Scanner scanner;

    ManipulateTxt(String path, Scanner scanner){
        //All operations will be performed on the file specified by this path
        textFile = new File(path);
        this.path = path;
        this.scanner = scanner;
    }

    private void getNumberOfLines(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            int lines = 0;
            while(reader.readLine() != null) lines++;
            reader.close();
            logMessage = "Counted " + lines + " lines in the file";
			} catch (FileNotFoundException e) {
                logMessage = "getNumberOfLines() - Could not find the file";
				e.printStackTrace();
            } catch (Exception e){
                logMessage = "getNumberOfLines() - Error";
                e.printStackTrace();
            }
    }

    private boolean containsWord(String word){
        BufferedReader reader;
		try {
            reader = new BufferedReader(new FileReader(path));
            String line;
            //Search for word in file, line by line
            while ((line = reader.readLine()) != null) {
                if(line.toLowerCase().contains(word.toLowerCase())){
                    reader.close();
                    logMessage = "Found word " + word;
                    return true;
                }
            }
            reader.close();
            logMessage = "The word " + word + " could not be found in the file";
            return false;
		} catch (FileNotFoundException e) {
            logMessage = "containsWord() - Could not find the file";
			e.printStackTrace();
        } catch(Exception e) {
            logMessage = "containsWord() - Error";
            e.printStackTrace();
        }
        return false;
    }

    private int countWord(String word){
        BufferedReader reader;
        int occurences = 0;
		try {
            reader = new BufferedReader(new FileReader(path));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.toLowerCase().split(" ");
                for( String textWord : words){
                    if(textWord.contains(word.toLowerCase())){
                        occurences++;
                    }
                }
            }
            reader.close();
            logMessage = "Found " + occurences + " occurences of the word " + word;
            return occurences;
		} catch (FileNotFoundException e) {
            logMessage = "countWord() - Could not find the file";
			e.printStackTrace();
        }  catch(Exception e) {
            logMessage = "countWord() - Error";
            e.printStackTrace();
        }
        return occurences;
    }

    //Create sub menu
    public void start(){
        boolean cancelled = false;
        long startTime;
        long endTime;
        do {
            System.out.println("What do you want to do with the text file? Please input the corresponding number");
            System.out.println("1. Get filename");
            System.out.println("2. Get filesize");
            System.out.println("3. Get number of lines in file");
            System.out.println("4. Check if word exists in file");
            System.out.println("5. Check how many times a word is found in the file");
            System.out.println("6. Go back");
            int choice = 0;
            try {
                choice = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Error: " + e);
                scanner.nextLine();
            }

            switch (choice) {
                case 1:
                    //Get the name of the file
                    startTime = System.nanoTime();
                    String fileName = textFile.getName();
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, "Retrieved name of file " + fileName);
                    break;
                case 2:
                    //Get the size of the file, in kilobytes
                    startTime = System.nanoTime();
                    long size = textFile.length()/(1024);
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, "Retrieved filesize: " + size + " kB");
                    break;
                case 3:
                    //Get the number of lines in the file
                    startTime = System.nanoTime();
                    getNumberOfLines();
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, logMessage);
                    System.out.println("\n");
                    break;
                case 4:
                    //Check if file contains a word
                    System.out.println("What word do you want to search for?");
                    scanner.nextLine();
                    String word = scanner.nextLine();
                    startTime = System.nanoTime();
                    if(containsWord(word)){
                        endTime = System.nanoTime();
                    }
                    else{
                        endTime = System.nanoTime();
                    }
                    logger.log(startTime, endTime, logMessage);
                    break;
                case 5:
                    //Count number of occurences of a word in the file
                    System.out.println("What word do you want to count?");
                    scanner.nextLine();
                    String searchWord = scanner.nextLine();
                    startTime = System.nanoTime();
                    countWord(searchWord);
                    endTime = System.nanoTime();
                    logger.log(startTime, endTime, logMessage);
                    break;
                case 6:
                    //Go back to main menu
                    cancelled = true;
                    break;
            
                default:
                    break;
            }
        } while(!cancelled);
    }


}