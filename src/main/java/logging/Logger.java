package main.java.logging;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    public void log(final long startTime, final long endTime, final String message){
        final long funcTimeMs = (endTime - startTime)/1000000;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        String log = formatter.format(date) + ": " + message + ". The function took " + funcTimeMs + " ms to execute";
        System.out.println(log);
        writeToLog(log);
    }

    private void writeToLog(String log){
        try {
            FileWriter writer = new FileWriter("../src/main/resources/logs/log.txt", true);
            writer.write(log + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
    }
}
