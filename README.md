# File system manager

This simple file system manager is written in Java and can interact with files in the resources directory of the project.

## Functionality

- List all files in the directory
- List files in the directory by extension
- Manipulate a .txt file in the folder
  - Get name of file
  - Get size of file
  - Check number of lines in the file
  - Check if a word exists in the file
  - Check how many times a word is found in the file
- Logging
  - The program also logs the result of every function above, along with a timestamp and the functions execution time.

### Compiling the project

![compilation command](/screenshots/compiling.png)

### Creating .jar file

![create jar command](/screenshots/createjar.png)

### Running .jar file

![run jar command](/screenshots/runjar.png)
